# Jonathan Voth
## Master of Science in Analytics

**What technical errors did you experience? Please list them explicitly and identify how you corrected them.**

I ran into many technical difficulties with my function to transforma the data. I was trying to convert the dataframe to an RDD, but an error occured about the first row of the RDD being empty. It could not "inferSchema". I think it was because the first row was the column names. Anyway, I ended up applying the function to each column individually, which worked for me. I also had to import "udf" and store my function as a user defined function. This was quite different from the video, but the result was the same. Once I transformed the data, the only other technical issues were syntax errors.

**What conceptual difficulties did you experience?**

I understood both the linear and logistic regression examples from a conceptual perspective. Proper implementation was the only issue with those examples. One conceptual difficulty I had with the k-means algorithm was the evaluation of the model. I understand that a k-means model splits the data into k clusters based on similarities; however, this is an unsupervised learning algorithm. How do you evaluate a model that has no given ouput? In our example, we had output but chose not to use it. In some examples I found online, the Silhouette coefficient was used to measure accuracy. This measures how similar objects are to its own cluster. Additionally, I was unsure on how many clusters to fit. I used k = 2, 3, 4 in my algorithm, but this may not be the most optimal number of clusters for this data.

I was able to navigate through the k-means algorithm using many resources, but I didn't necessarily understand exactly what I was doing along the way.

**How much time did you spend on each part of the assignment?**

Overall, I probably spent about 10-12 hours on this assignment. The first task took the shortest amount of time. I only needed like 1-2 hours to complete the linear regression model. The logistic regression model took the most time, probably somewhere in the range of 3-4 hours. I spent the most time writing the function and applying it to the dataframe to tranform that data. The k-means algorithm was in the middle as far as time is concerned. I used a lot of the same functions from the logistic regression, so it didn't take as much time.

I had no issues running docker or using Gitlab. Those aspects took minimal time. And of course, answering the questions in the README added some time at the end.

**What was the hardest part of this assignment?**

The first difficulty I had with this reflection was transforming the data to double values for the logistic regression. I have never actually written my own function in Python before. The syntax is not the same as Scala, so it took a while for me to get it to run. Next, I also had a hard time applying the function to the dataframe. I ended up applying it to each individual column instead of the whole dataframe. This probably isn't the most efficient way to do it, but the only way that worked for me. Moreover, I struggled to properly evaluate the k-means model. I used WSSSE which stands for "Within Set Sum of Squared Errors". This measure is the sum of the squared distances between each data point and the centroid. A good model would attempt to minimize WSSSE. As seen in my model, more clusters decreased that distance but having a high number of clusters is probably bad too. The optimal model will balance these two metrics.

**What was the easiest part of this assignment?**

The easiest part of the assignment was fitting the linear regression model. Everything was pretty straight forward from the tutorial video. I also found other resources online and used the book to help me do a few of the evaluation queries. Additionally, I am in a Data Mining course in which we are doing very similar things but in R. The concepts and model fitting steps are the same. Having past experience fitting models, even in a different language and environment, helped me throughout this reflection. The logistic and k-means models were slightly more difficult to fit.

**What advice would you give someone doing this assignment in the future?**

My advice is similar to previous weeks. I find I have the most success when I take the reflection one step at a time. This week, there were three different tasks to do. I worked through each of them separately and only moved on to the next when the previous was finished. Additionally, I would advise someone to use their resources. There are plenty of examples online to look at and use to help you. I don't think I could've done this reflection without using all of the resources provided. My last advice would be to use previous knowledge. For me, model fitting is not a new concept. I used my past knowledge and experience to work through these tasks. Everything builds on previous weeks and even previous classes build upon one another. Use the knowledge that you've accumulated, and apply it to the reflection.

**What did you actually learn from doing this assignment?**

This was my first experience with machine learning. Machine learning is under the umbrella of artificial intelligence. I never really thought of model fitting as machine learning, but it makes sense. Other common examples of machine learning uses are identifying spam emails and suggested Google searches. Machine learning is all around us. Pertaining to this reflection specifically, I learned how to fit three different types of models: linear, logistic, and k-means. The first two are supervised learning models while k-means is considered unsupervised learning. This is my first experience writing a k-means algorithm, so I am still learning a lot about it. This week was a good first step.

**Why does what I learned matter both academically and practically?**

Artificial intelligence is an emerging field. We are now seeing things like self-driving cars which is crazy to think about. Machine learning falls under that AI umbrella. As someone who thought they had no experience in machine learning, I learned a lot this week. I am used to R for model fitting, but it was good to add Spark and Python to that list of environments. In my previous experience with model fitting, I used relatively small data sets. However, in the professional world, it is unlikely that I will be working with less than 1000 data points. Big Data is a whole different world, but more practical to learn. This experience and knowledge will be useful as I move into the professional world. Spark is a wonderful tool that I am learning more about each week.